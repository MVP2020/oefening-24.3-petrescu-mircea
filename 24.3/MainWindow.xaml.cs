﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace _24._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Werknemer> employees = new List<Werknemer>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Werknemer employee = new Werknemer(Achternaam.Text, Voornaam.Text, Convert.ToDouble(Verdiensten.Text));
            employees.Add(employee);
            Show.Text = employee.ToonAlles();

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
