﻿using System;

namespace _24._3
{
    class Werknemer
    {
        private string _naam;
        private string _voornaam;

        private double _verdiensten;

        public string Naam { get; set; }

        public string Voornaam { get; set; }

        public double Verdiensten { get; set; }
        public Werknemer(string naam, string voornaam, double verdiensten)
        {
            Naam = naam;
            Voornaam = voornaam;
            Verdiensten = verdiensten;
        }
        public string ToonAlles()
        {
            return Naam + "                            " + Voornaam + "                            " + Verdiensten + "€" + Environment.NewLine;
        }
    }
}
